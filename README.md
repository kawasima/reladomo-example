# Reladomo Example

Reladomoのサンプルの動かし方です。

### 1. 設定を書きます

Reladomoは設定ファイルからクラスをジェネレートするので、まず `src/main/resources` のファイルを作ります。

このExampleでは、 `src/main/resources/reladomo/models` の下に置いています。
`ReladomoClassList.xml` はReladomoで生成するモデルのインデックスです。

ReladomoClassListの中のMithraObjectResourceが、Reladomoで使うモデルでここでは、PersonとObjectSequenceの2つ
が定義されています。定義の詳細は、 `Person.xml` と`ObjectSequence.xml` にあります。

`reladomoobject.xsd` は、モデル定義のXML自体の形式定義で、ここにおいておくとIntellij IDEAなどのIDEでXML開いたときに、
エラー表示されないようになります。

この設定を書き終えた段階で、Reladomoのジェネレータを動かして、モデルクラスを生成します。

このジェネレータは、 `pom.xml` でmvnのライフサイクルに仕込んであるので、compileのライフサイクルを実行すると、
ソースコードが生成されます。

```
% mvn compile
```

IDEで実行した場合は、mavenプロジェクトの再読込を実行してください。

### 2. 生成したモデルを使ってアプリケーションを動かす。

`src/ext/java` にモデルへの追記部分とApplicationのメインファイルをおいてあるので、
これを `src/main/java` にコピってください。

```shell
% cp -r src/ext/java/sample/domain src/main/java/sample                                                                    main ✚ ✱ ◼
% cp -r src/ext/java/sample/util src/main/java/sample                                                                      main ✚ ✱ ◼
% cp -r src/ext/java/sample/HelloReladomoApp.java src/main/java/sample
```

これで、 `HelloReladomoApp` を動かせば、DDL実行しデータをInsertし、ReladomoからSELECTされる様子が見れます。

なお、Reladomoのモデルはジェネレーションギャップパターンになっており、src/main/javaの下に生成されるファイルは
コンストラクタ部を除いて、自由に編集してよいことになっています。
編集不可の部分は親クラスのPersonAbstractなどに書かれており、これは `target/generated-sources/reladomo` の下に生成されます。
