package sample.domain;
public class PersonAttribute extends PersonAttributeAbstract
{
	public PersonAttribute()
	{
		super();
		// You must not modify this constructor. Mithra calls this internally.
		// You can call this constructor. You can also add new constructors.
	}

	public static PersonAttribute createPersonAttribute(String setId, AttributeType attributeType, String value) {
		PersonAttribute personAttribute = new PersonAttribute();
		personAttribute.setAttributeType(attributeType);
		personAttribute.setSetId(setId);
		personAttribute.setValue(value);
		personAttribute.insert();
		return personAttribute;
	}

	@Override
	public String toString() {
		return "{ type=" + getAttributeType() + ", setId=" + getSetId() + ", value=" + getValue() + " }";
	}
}
