package sample.domain;

public class AttributeType extends AttributeTypeAbstract
{
	public AttributeType()
	{
		super();
		// You must not modify this constructor. Mithra calls this internally.
		// You can call this constructor. You can also add new constructors.
	}

	public static AttributeType create(long id, String name)
	{
		AttributeType attributeType = new AttributeType();
		attributeType.setId(id);
		attributeType.setName(name);
		attributeType.insert();
		return attributeType;
	}

	@Override
	public String toString() {
		return getName();
	}
}
