package sample.domain;
import com.gs.fw.finder.Operation;
import java.util.*;
public class AttributeTypeList extends AttributeTypeListAbstract
{
	public AttributeTypeList()
	{
		super();
	}

	public AttributeTypeList(int initialSize)
	{
		super(initialSize);
	}

	public AttributeTypeList(Collection c)
	{
		super(c);
	}

	public AttributeTypeList(Operation operation)
	{
		super(operation);
	}
}
