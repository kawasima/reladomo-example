package sample.domain;
import com.gs.fw.finder.Operation;
import java.util.*;
import java.util.stream.Collectors;

public class PersonAttributeList extends PersonAttributeListAbstract
{
	public PersonAttributeList()
	{
		super();
	}

	public PersonAttributeList(int initialSize)
	{
		super(initialSize);
	}

	public PersonAttributeList(Collection c)
	{
		super(c);
	}

	public PersonAttributeList(Operation operation)
	{
		super(operation);
	}

	@Override
	public String toString() {
		return "[" + stream().map(PersonAttribute::toString).collect(Collectors.joining(",")) + "]";
	}
}
