package sample.domain;

import com.gs.fw.common.mithra.finder.Operation;
import com.gs.fw.common.mithra.util.DefaultInfinityTimestamp;
import de.huxhorn.sulky.ulid.ULID;

import java.sql.Timestamp;
import java.util.Map;

public class Person extends PersonAbstract
{
	public Person(Timestamp ts)
	{
		super(ts);
		// You must not modify this constructor. Mithra calls this internally.
		// You can call this constructor. You can also add new constructors.
	}

	public static Person createPerson(Map<AttributeType, String> attributes)
	{
		Person person = new Person(DefaultInfinityTimestamp.getDefaultInfinity());
		person.setAttributeSetId(new ULID().nextULID());
		person.insert();

		attributes.entrySet().stream().forEach(e -> {
			PersonAttribute personAttribute = PersonAttribute.createPersonAttribute(person.getAttributeSetId(), e.getKey(), e.getValue());
		});
		return person;
	}

	public static Person findPerson()
	{
		Operation findByFirstNameAndLastName =
				PersonFinder.all();
		return PersonFinder.findOne(findByFirstNameAndLastName);
	}

	public void update(Map<AttributeType, String> attributes) {
		setAttributeSetId(new ULID().nextULID());
		getAttributes().clear();
		attributes.entrySet().stream().forEach(e -> {
			PersonAttribute.createPersonAttribute(getAttributeSetId(), e.getKey(), e.getValue());
		});
	}

	@Override
	public String toString() {
		return "{ id=" + getPersonId() + ",attributes=" + getAttributes() + "}";
	}
}
