package sample;

import com.gs.fw.common.mithra.MithraManager;
import com.gs.fw.common.mithra.MithraManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sample.domain.AttributeType;
import sample.domain.Person;
import sample.util.H2ConnectionManager;

import java.io.InputStream;
import java.util.Map;

public class ReladomoApp {
    private static Logger logger = LoggerFactory.getLogger(ReladomoApp.class.getName());
    private static final int MAX_TRANSACTION_TIMEOUT = 120;

    public static void main(String[] args)
    {
        ReladomoApp app = new ReladomoApp();
        initialize(app);

        AttributeType firstNameAttr = AttributeType.create(1L, "firstName");
        AttributeType lastNameAttr = AttributeType.create(2L, "lastName");
        AttributeType countryAttr = AttributeType.create(3L, "country");

        MithraManagerProvider.getMithraManager().executeTransactionalCommand(tx -> {
            return Person.createPerson(Map.of(
                    firstNameAttr, "Taro",
                    lastNameAttr, "Tanaka",
                    countryAttr, "JPN"
            ));
        });

        Person tanakaTaro = Person.findPerson();
        logger.info("Inserted " + tanakaTaro.getAttributes());

        MithraManagerProvider.getMithraManager().executeTransactionalCommand(tx -> {
            tanakaTaro.update(Map.of(
                    firstNameAttr, "Taro",
                    lastNameAttr, "Tanaka",
                    countryAttr, "USA"
            ));
            return null;
        });
        logger.info("Updated " + Person.findPerson());

        H2ConnectionManager.getInstance().dump();
    }

    private static void initialize(ReladomoApp app)
    {
        try
        {
            // This line is added to make in-memory tables ready.
            // It's not typically done in production app that usually connects to physical db
            H2ConnectionManager.getInstance().prepareTables();

            app.initialiseReladomo();
            app.loadReladomoConfigurationXml("reladomo/config/ReladomoRuntimeConfig.xml");
        }
        catch(Exception e)
        {
            logger.error("Couldn't run HelloReladomoApp.", e);
        }
    }

    private void initialiseReladomo() throws Exception
    {
        try
        {
            logger.info("Transaction Timeout is " + MAX_TRANSACTION_TIMEOUT);
            MithraManager mithraManager = MithraManagerProvider.getMithraManager();
            mithraManager.setTransactionTimeout(MAX_TRANSACTION_TIMEOUT);
            // Notification should be configured here. Refer to notification/Notification.html under reladomo-javadoc.jar.
        }
        catch (Exception e)
        {
            logger.error("Unable to initialise Reladomo!", e);
            throw new Exception("Unable to initialise Reladomo!", e);
        }
        logger.info("Reladomo has been initialised!");
    }

    private void loadReladomoConfigurationXml(String reladomoRuntimeConfig) throws Exception
    {
        logger.info("Reladomo configuration XML is " + reladomoRuntimeConfig);
        InputStream is = ReladomoApp.class.getClassLoader().getResourceAsStream(reladomoRuntimeConfig);
        if (is == null) throw new Exception("can't find file: " + reladomoRuntimeConfig + " in classpath");
        MithraManagerProvider.getMithraManager().readConfiguration(is);
        try
        {
            is.close();
        }
        catch (java.io.IOException e)
        {
            logger.error("Unable to initialise Reladomo!", e);
            throw new Exception("Unable to initialise Reladomo!", e);
        }
        logger.info("Reladomo configuration XML " + reladomoRuntimeConfig+" is now loaded.");
    }}
