package sample.util;

import com.gs.fw.common.mithra.MithraSequence;
import com.gs.fw.common.mithra.MithraSequenceObjectFactory;
import sample.domain.ObjectSequence;
import sample.domain.ObjectSequenceFinder;

public class ObjectSequenceObjectFactory implements MithraSequenceObjectFactory {
    public MithraSequence getMithraSequenceObject(String sequenceName, Object sourceAttribute, int initialValue)
    {
        ObjectSequence objectSequence = ObjectSequenceFinder.findByPrimaryKey(sequenceName);
        if (objectSequence == null)
        {
            objectSequence = new ObjectSequence();
            objectSequence.setSequenceName(sequenceName);
            objectSequence.setNextId(initialValue);
            objectSequence.insert();
        }
        return objectSequence;
    }
}
